package com.learning.alext.a3lers.mvp.model;

public class TrailerModel {

    private String thumbnailUrl;
    private String title;

    public TrailerModel(String thumbnailUrl, String title) {
        this.thumbnailUrl = thumbnailUrl;
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }
}
