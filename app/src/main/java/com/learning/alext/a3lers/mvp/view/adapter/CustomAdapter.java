package com.learning.alext.a3lers.mvp.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.learning.alext.a3lers.mvp.model.MovieResult;

import java.util.List;

public abstract class CustomAdapter extends RecyclerView.Adapter {

    List<?> items;
    LayoutInflater layoutInflater;
    OnItemClickListener onItemClickListener;
    Context context;


    CustomAdapter(List<?> movies, Context context, OnItemClickListener onItemClickListener) {
        this.items = movies;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public interface OnItemClickListener{
        void onItemClicked(int pos, View view);

    }

    void setSlideAnimation(View view){
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        view.startAnimation(animation);
    }
}
