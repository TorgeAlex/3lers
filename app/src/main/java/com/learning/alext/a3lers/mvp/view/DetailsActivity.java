package com.learning.alext.a3lers.mvp.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.learning.alext.a3lers.R;
import com.learning.alext.a3lers.mvp.model.TrailerModel;
import com.learning.alext.a3lers.mvp.presenter.DetailsPresenter;
import com.learning.alext.a3lers.mvp.view.adapter.CustomAdapter;
import com.learning.alext.a3lers.mvp.view.adapter.TrailerAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity implements DetailView, CustomAdapter.OnItemClickListener {

    @BindView(R.id.detail_image_view)
    ImageView movieImageV;

    @BindView(R.id.movie_title_tv)
    TextView movieTitleV;

    @BindView(R.id.trailers_list_view)
    RecyclerView trailerListView;

    RecyclerView.Adapter adapter;

    DetailsPresenter detailsPresenter;

    String movieTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailsPresenter = new DetailsPresenter(this);
        initView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        detailsPresenter.onConfigurationChanged();
    }

    @Override
    public void initView() {
        setContentView(R.layout.details_activity);
        ButterKnife.bind(this);
        String imageUrl = getResources().getString(R.string.image_base_url)
                + getIntent().getStringExtra(getResources().getString(R.string.url));
        Picasso.get().load(imageUrl).into(movieImageV);
        movieTitle = getIntent().getStringExtra(getResources().getString(R.string.movie_name));
        movieTitleV.setText(movieTitle);
        showTrailerList();
    }

    /**
     * Shows the trailers list view and populates it
     */
    private void showTrailerList() {
        List<TrailerModel> trailerModels = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            //first 4 thumbnails are visible only for this video
            String thumbnailUrl = getResources().getString(R.string.thumbnail_url)
                    + (i % 4) + getResources().getString(R.string.extension);
            String title = movieTitle + getResources().getString(R.string.trailer) + (i+1);
            trailerModels.add(new TrailerModel(thumbnailUrl,title));
        }
        adapter = new TrailerAdapter(trailerModels, this, this);
        trailerListView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        trailerListView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onItemClicked(int pos, View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=9fyI-SEnNl8")));
    }
}
