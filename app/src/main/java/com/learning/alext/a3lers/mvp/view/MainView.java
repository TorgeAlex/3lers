package com.learning.alext.a3lers.mvp.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import com.learning.alext.a3lers.mvp.model.MovieResult;

import java.util.List;

public interface MainView {

    Context getContext();

    void setMovies(List<MovieResult> results);

    void showLoadingIndicator();

    void showNoResultsView();

    void showNoNetworkMessage();

    void registerNetworkReceiver(BroadcastReceiver receiver);

    void unregisterNetworkReceiver(BroadcastReceiver receiver);
}
