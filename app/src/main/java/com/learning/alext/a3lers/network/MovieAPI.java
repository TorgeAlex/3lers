package com.learning.alext.a3lers.network;

import com.learning.alext.a3lers.mvp.model.MovieRequestResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieAPI {

    @GET("top_rated?api_key=22728da92b2888f87edf83b80a5cc28e&language=en-US/")
    Call<MovieRequestResponse> getMovies();

}
