package com.learning.alext.a3lers.mvp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.learning.alext.a3lers.R;
import com.learning.alext.a3lers.mvp.model.TrailerModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TrailerAdapter extends CustomAdapter {

    public TrailerAdapter(List<?> items, Context context, OnItemClickListener onItemClickListener) {
        super(items, context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.trailer_list_item, viewGroup, false);
        return new TrailerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        TrailerModel trailerModel = (TrailerModel) items.get(i);
        ((TrailerViewHolder) viewHolder).nameTv.setText(trailerModel.getTitle());
        Picasso.get()
                .load(trailerModel.getThumbnailUrl())
                .into(((TrailerViewHolder) viewHolder).imageView);
    }

    class TrailerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.trailer_name_tv)
        TextView nameTv;

        @BindView(R.id.trailer_image_view)
        ImageView imageView;

        @BindView(R.id.root_layout)
        View rootLayout;

        TrailerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClicked(getAdapterPosition(), imageView);
                }
            });
            setSlideAnimation(rootLayout);
        }
    }
}
