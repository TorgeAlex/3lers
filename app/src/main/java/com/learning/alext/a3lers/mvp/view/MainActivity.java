package com.learning.alext.a3lers.mvp.view;

import android.content.*;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.learning.alext.a3lers.R;
import com.learning.alext.a3lers.apputils.AppUtils;
import com.learning.alext.a3lers.mvp.model.MovieResult;
import com.learning.alext.a3lers.mvp.presenter.MainPresenter;
import com.learning.alext.a3lers.mvp.view.adapter.CustomAdapter;
import com.learning.alext.a3lers.mvp.view.adapter.ImageAdapter;
import com.learning.alext.a3lers.mvp.view.adapter.ListAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView, CustomAdapter.OnItemClickListener {

    private static final int IMAGE_VIEW = 0;
    private static final int LIST_VIEW = 1;


    private MainPresenter mainPresenter;
    @BindView(R.id.loading_indicator)
    ProgressBar loadingIndicator;

    @BindView(R.id.no_results_found_tv)
    TextView noResultsFoundTV;

    @BindView(R.id.movie_list)
    RecyclerView movieListView;

    @BindView(R.id.no_internet_access)
    TextView noInternetAccessTv;

    CustomAdapter adapter;

    LinearLayoutManager linearLayoutManager;

    int displayMode = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        mainPresenter = new MainPresenter(this);

        showPreferenceDialog();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void registerNetworkReceiver(BroadcastReceiver receiver) {
        registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void unregisterNetworkReceiver(BroadcastReceiver receiver) {
        unregisterReceiver(receiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mainPresenter.onViewPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.onViewResumed();
    }

    @Override
    public void setMovies(List<MovieResult> results) {
        noResultsFoundTV.setVisibility(View.GONE);
        movieListView.setVisibility(View.VISIBLE);
        loadingIndicator.setVisibility(View.GONE);
        noInternetAccessTv.setVisibility(View.GONE);
        switch (displayMode) {
            case LIST_VIEW:
                showListView();
                break;
            case IMAGE_VIEW:
                showImageView();
                break;
            default:
                break;

        }
    }

    @Override
    public void showNoNetworkMessage() {
        noResultsFoundTV.setVisibility(View.GONE);
        movieListView.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.GONE);
        noInternetAccessTv.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingIndicator() {
        noResultsFoundTV.setVisibility(View.GONE);
        noInternetAccessTv.setVisibility(View.GONE);
        movieListView.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNoResultsView() {
        noResultsFoundTV.setVisibility(View.VISIBLE);
        noInternetAccessTv.setVisibility(View.GONE);
        movieListView.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        mainPresenter.onOrientationChange();
    }

    @Override
    public void onItemClicked(int pos, View view) {
        Intent intent = new Intent(this, DetailsActivity.class);
        MovieResult movieResult = mainPresenter.getMovies().get(pos);
        intent.putExtra(getResources().getString(R.string.url), movieResult.getPosterPath());
        intent.putExtra(getResources().getString(R.string.movie_name), movieResult.getTitle());
        intent.putExtra(getResources().getString(R.string.votes),
                AppUtils.getFormatedVotesString(movieResult.getVoteAverage()
                        , movieResult.getVoteCount(), this));
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, view, view.getTransitionName());
        startActivity(intent, options.toBundle());
    }


    private void showPreferenceDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Display the content in:")
                .setPositiveButton("List View", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        displayMode = LIST_VIEW;
                        if (mainPresenter.getMovies() != null) {
                            showListView();
                        }
                    }
                })
                .setNegativeButton("Image View", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        displayMode = IMAGE_VIEW;
                        if (mainPresenter.getMovies() != null) {
                            showImageView();
                        }
                    }
                });
        // Create the AlertDialog object and return it
        builder.setCancelable(false);
        builder.create().show();
    }

    private void showImageView() {
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        adapter = new ImageAdapter(mainPresenter.getMovies(), this, this);
        movieListView.setAdapter(adapter);
        movieListView.setLayoutManager(linearLayoutManager);
    }

    private void showListView() {
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new ListAdapter(mainPresenter.getMovies(), this, this);
        movieListView.setAdapter(adapter);
        movieListView.setLayoutManager(linearLayoutManager);
    }
}
