package com.learning.alext.a3lers.apputils;

import android.content.Context;
import com.learning.alext.a3lers.R;

public class AppUtils {

    public static String getFormatedVotesString(double grade, int voteCount, Context context){
        return grade + context.getResources().getString(R.string.separator)
                + voteCount + context.getResources().getString(R.string.votes);
    }
}
