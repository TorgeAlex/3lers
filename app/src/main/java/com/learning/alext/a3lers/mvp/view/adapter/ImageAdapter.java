package com.learning.alext.a3lers.mvp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.learning.alext.a3lers.R;
import com.learning.alext.a3lers.mvp.model.MovieResult;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends CustomAdapter {

    public ImageAdapter(List<MovieResult> movies, Context context, OnItemClickListener onItemClickListener) {
        super(movies, context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.image_view_item, viewGroup, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        MovieResult movieResult = (MovieResult) items.get(i);
        ((ImageViewHolder) viewHolder).nameTv.setText(movieResult.getTitle());
        Picasso.get()
                .load(context.getResources().getString(R.string.image_base_url) + movieResult.getPosterPath())
                .into(((ImageViewHolder) viewHolder).imageView);
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_name_tv)
        TextView nameTv;

        @BindView(R.id.image_view)
        ImageView imageView;

        @BindView(R.id.root_layout)
        View rootLayout;

        ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClicked(getAdapterPosition(), imageView);
                }
            });
            setSlideAnimation(rootLayout);
        }
    }

}
