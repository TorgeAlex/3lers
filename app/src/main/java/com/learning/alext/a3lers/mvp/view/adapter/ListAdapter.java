package com.learning.alext.a3lers.mvp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.learning.alext.a3lers.R;
import com.learning.alext.a3lers.apputils.AppUtils;
import com.learning.alext.a3lers.mvp.model.MovieResult;

import java.util.List;

public class ListAdapter extends CustomAdapter {

    public ListAdapter(List<MovieResult> movies, Context context, OnItemClickListener onItemClickListener) {
        super(movies, context, onItemClickListener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.list_view_item, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        MovieResult movieResult = (MovieResult) items.get(i);
        ((ListViewHolder) viewHolder).nameTv.setText(movieResult.getTitle());
        ((ListViewHolder) viewHolder).idTv.setText(AppUtils.getFormatedVotesString(movieResult.getVoteAverage(),movieResult.getVoteCount(),context));
    }

    class ListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.list_name_tv)
        TextView nameTv;

        @BindView(R.id.list_id_tv)
        TextView idTv;

        @BindView(R.id.root_layout)
        View rootLayout;

        ListViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClicked(getAdapterPosition(),rootLayout);
                }
            });
            setSlideAnimation(rootLayout);
        }
    }
}
