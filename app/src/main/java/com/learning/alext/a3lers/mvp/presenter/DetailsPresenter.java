package com.learning.alext.a3lers.mvp.presenter;

import com.learning.alext.a3lers.mvp.view.DetailView;

public class DetailsPresenter {

    private DetailView detailView;

    public DetailsPresenter(DetailView detailView) {
        this.detailView = detailView;
    }

    /**
     * Handles orientation changes
     */
    public void onConfigurationChanged() {
        detailView.initView();
    }
}
