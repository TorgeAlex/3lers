package com.learning.alext.a3lers.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetworkReceiver extends BroadcastReceiver {

    private OnInternetConnectionChangedListener listener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (listener != null) {
            listener.onInternetConnectionChanged();
        }
    }

    public void setListener(OnInternetConnectionChangedListener listener) {
        this.listener = listener;
    }

    public interface OnInternetConnectionChangedListener {
        void onInternetConnectionChanged();
    }

}
