package com.learning.alext.a3lers.mvp.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import com.learning.alext.a3lers.network.NetworkReceiver;
import com.learning.alext.a3lers.mvp.model.MovieRequestResponse;
import com.learning.alext.a3lers.mvp.model.MovieResult;
import com.learning.alext.a3lers.mvp.view.MainView;
import com.learning.alext.a3lers.network.MovieAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

public class MainPresenter implements NetworkReceiver.OnInternetConnectionChangedListener {

    private static final String BASE_URL = "https://api.themoviedb.org/3/movie/";

    private MainView mainView;
    private boolean isDownloadStarted;
    private MovieRequestResponse requestResponse;
    private NetworkReceiver networkReceiver;

    public MainPresenter(MainView mainView) {
        networkReceiver = new NetworkReceiver();
        networkReceiver.setListener(this);
        this.mainView = mainView;
    }

    @Override
    public void onInternetConnectionChanged() {
        if (shouldInitRequest()) {
            initRequest();
        } else if(isDownloadStarted || getMovies()==null){
            mainView.showNoNetworkMessage();
        }
    }

    /**
     * Returns a list of Movie Result
     */
    public List<MovieResult> getMovies() {
        if (requestResponse != null) {
            return requestResponse.getResults();
        }
        return null;
    }

    public void onViewResumed() {
        mainView.registerNetworkReceiver(networkReceiver);
    }

    public void onViewPaused() {
        mainView.unregisterNetworkReceiver(networkReceiver);
    }

    /**
     * Initializes the request
     */
    private void initRequest() {
        isDownloadStarted = true;
        mainView.showLoadingIndicator();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MovieAPI movieAPI = retrofit.create(MovieAPI.class);
        movieAPI.getMovies().enqueue(new Callback<MovieRequestResponse>() {
            @Override
            public void onResponse(@NonNull Call<MovieRequestResponse> call, @NonNull Response<MovieRequestResponse> response) {
                isDownloadStarted = false;
                if (response.isSuccessful() && response.body() != null) {
                    updateMovieList(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<MovieRequestResponse> call, @NonNull Throwable t) {
                isDownloadStarted = false;
                mainView.showNoResultsView();
            }
        });
    }

    /**
     * Updates the movie list
     *
     * @param requestResponse
     */
    private void updateMovieList(MovieRequestResponse requestResponse) {
        this.requestResponse = requestResponse;
        mainView.setMovies(requestResponse.getResults());
    }

    /**
     * Checks if the network is online
     *
     * @return
     */
    private boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    /**
     * Checks whether all the conditions are met for the movies request to be made
     *
     * @return
     */
    private boolean shouldInitRequest() {
        return (!isDownloadStarted && requestResponse == null
                && isOnline(mainView.getContext()));
    }

    /**
     * Handles orientation changes
     */
    public void onOrientationChange() {
        if (requestResponse != null) {
            mainView.setMovies(requestResponse.getResults());
        }
    }
}